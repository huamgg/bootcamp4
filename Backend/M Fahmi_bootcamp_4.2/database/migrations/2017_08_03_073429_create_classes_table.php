<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->increments('class_id');
            $table->string('nama_class');

            $table->integer('course_id')->unsigned();
            $table->foreign('course_id')->references('course_id')->on('courses');

            $table->integer('no_mahasiswa')->unsigned();
            $table->foreign('no_mahasiswa')->references('no_mahasiswa')->on('mahasiswas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
