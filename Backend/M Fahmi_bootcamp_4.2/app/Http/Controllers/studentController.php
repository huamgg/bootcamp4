<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\mahasiswa;

class studentController extends Controller
{
    function getDataStudent(){

        DB::beginTransaction();

        try{
            $studentlist = mahasiswa::get();

            return response()->json($studentlist, 201);
        }

        catch(\Exception $e){
            DB::rollback();
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    function saveEditDatastudent(Request $request){

        DB::beginTransaction();

        try{
            
            $this->validate($request, [
                'nama_mhs' => 'required',
                'alamat' => 'required',
                'no_telp' => 'required'
            ]);

            $nama_mhs = $request->input('nama_mhs');
            $alamat = $request->input('alamat');
            $no_telp = $request->input('no_telp');
            $email = $request->input('email');
            
            if (isset($request->no_mahasiswa)){
                
                $data = mahasiswa::find($request->no_mahasiswa);
                $data->nama_mhs = $nama_mhs;
                $usr->alamat = $alamat;
                $usr->no_telp = $no_telp;
                $usr->email = $email;
                $usr->save();
            }
            else{

            $usr = new mahasiswa;
            $usr->nama_mhs = $nama_mhs;
            $usr->alamat = $alamat;
            $usr->no_telp = $no_telp;
            $usr->email = $email;
            $usr->save();
            }
            
            $studentlist = mahasiswa::get();

            DB::commit();
            return response()->json($studentlist, 201);

        }
        catch(\Exception $e){
            DB::rollback();
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

}
