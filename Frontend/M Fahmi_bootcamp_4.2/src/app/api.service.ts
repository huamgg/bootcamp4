import { Injectable } from '@angular/core';

@Injectable()
export class ApiService {

  constructor() { }

  private course : object[] = [
    {"course_id" : 1001, "nama_course" : "matematika", "pengajar" : "Maemunah"},
    {"course_id" : 1002, "nama_course" : "fisika", "pengajar" : "Yandi"},
    {"course_id" : 1003, "nama_course" : "kimia", "pengajar" : "Budiman"}
  ]

  getCourseList():object[]
  {
    return this.course;
  }

}
