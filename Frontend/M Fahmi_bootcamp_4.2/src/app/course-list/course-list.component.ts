import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  form : object[];

  constructor(private api:ApiService) { }

  ngOnInit() {
    this.form = this.api.getCourseList();
  }

  newcourse_id : number;
  newnama_course : string = "";
  newpengajar : string = "";
  

  addToList(){
    // untuk mencari ID
    if (this.form.length == 0) {
      this.newcourse_id = 1;
    }
    else{
      this.newcourse_id = this.form[this.form.length - 1]['course_id']+1;
    }

    // untuk tidak menambah jika ada yang kosong
    if (this.newnama_course != "" && this.newpengajar != "") {

      this.form.push({"course_id":this.newcourse_id, "nama_course":this.newnama_course, "pengajar":this.newpengajar});
      }      
    }

}
